<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div class="container">
 *
 * @package SKT Cafe
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- <link rel="stylesheet" type="text/css" href="style.css"> -->
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!--HEADER STARTS-->
<?php 
	$contact_no = get_theme_mod('contact_no'); 
	$contact_top_address = get_theme_mod('contact_top_address'); 
	$hidetopbar = get_theme_mod('hide_header_topbar', 1);
if( $hidetopbar == '') { ?>
<div class="head-info-area">
<div class="center">
<div class="left">
	        <?php if(!empty($contact_top_address)){?>
		 <span class="phntp">
          <span class="phoneno"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon-address.png" alt="" /> 
          <?php echo esc_html($contact_top_address); ?></span>
        </span>
        <?php } ?> 
</div> 
		<?php if(!empty($contact_no)){?>
		<div class="right">
        	<span class="emltp">
			<?php echo esc_html($contact_no); ?>
            </span>
            </div>
        <?php } ?> 
<div class="clear"></div>                
</div>
</div>
<?php } ?>
<!--HEADER ENDS-->
<div class="header">
  <div class="container" style="display: none;">
    <div class="logo">
		<?php skt_cafe_the_custom_logo(); ?>
        <div class="clear"></div>
		<?php	
        $description = get_bloginfo( 'description', 'display' );
        ?>
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
        <h2 class="site-title"><?php bloginfo('name'); ?></h2>
        <?php if ( $description || is_customize_preview() ) :?>
        <p class="site-description"><?php echo esc_html($description); ?></p>                          
        <?php endif; ?>
        </a>
    </div>
         <div class="toggle"><a class="toggleMenu" href="#" style="display:none;"><?php esc_attr_e('Menu','skt-cafe'); ?></a></div> 
        <div class="sitenav">
          <?php wp_nav_menu( array('theme_location' => 'primary') ); ?>         
        </div><!-- .sitenav--> 
        <div class="clear"></div> 
  </div> <!-- container -->
<div class="header-imgs">

  <img class="mySlides" src="https://01tqm9cgm5-flywheel.netdna-ssl.com/wp-content/uploads/2013/03/pizza-la-stella-1997-Edit-1-1500x1000.jpg" alt="header-img1">
  <img class="mySlides" src="https://pizzalastella.com/wp-content/uploads/2013/03/pizza-la-stella-1523-1-1500x1000.jpg" alt="header-img2">
  <img class="mySlides" src="https://01tqm9cgm5-flywheel.netdna-ssl.com/wp-content/uploads/2013/03/pizza-la-stella-071-Edit-1-1500x1000.jpg" alt="header-img3">

</div>

<div class="top-menu">
  <span onclick="sideNavigation();"></span>
</div>

<?php

wp_nav_menu(array(
    'theme_location' => 'menu_location', 
    'menu' => 'top-menu',
    'container' => false,
    'menu_class' => 'sidenav', 
    'menu_id' => 'mySidenav'
 ));
?>

<div class="logo-img">

  <img src="https://01tqm9cgm5-flywheel.netdna-ssl.com/wp-content/uploads/2018/08/pls-logo-tagline.png" alt="logo-img">

</div>

<div class="header-scroll">
</div>
<div class="social-icons">

  <a href="https://www.facebook.com/pizzalastellaUS/?fref=ts" title="Facebook" target="_blank"><i class="fab fa-facebook-f"></i></a>
  <a href="https://twitter.com/PizzaLaStellaUS" title="Twitter" target="_blank"><i class="fab fa-twitter"></i></a>
  <a href="https://instagram.com/pizzalastellaus" title="Instagram" target="_blank"><i class="fab fa-instagram"></i></a>
  <a href="https://www.youtube.com/channel/UCatckMHo9aOJcX1ZJBQ7clQ" title="Youtube" target="_blank"><i class="fab fa-youtube"></i></a>

</div>

<div class="header-btn">

  <a href="https://pizzalastella.com/delivery/"><button type="button">delivery</button></a>
  <span>|</span>
  <a href="https://www.opentable.com/r/pizza-la-stella-reservations-raleigh?rid=987769&restref=987769" target="_blank"><button type="button">make a reservation</button></a>

</div>

    <script>

      // change header images 
      var myIndex = 0;
      carousel();
      function carousel() {
        var i;
        var x = document.getElementsByClassName("mySlides");
        for (i = 0; i < x.length; i++) {
          x[i].style.display = "none";  
        }

        myIndex++;
        if (myIndex > x.length) {
          myIndex = 1
        }    
        x[myIndex-1].style.display = "block";  
        setTimeout(carousel, 3000);
      }

      var menuIcon = document.querySelector('span');

      function toggleMenu() {
        menuIcon.classList.toggle('close');
      } 

      menuIcon.addEventListener('click', toggleMenu);

      function sideNavigation() {

        var navbar = document.getElementById("mySidenav");

        if (navbar.style.width == "350px") {

          navbar.style.width = "0px";
        } else {

          navbar.style.width = "350px";
        }
      }

      // Show top header on scroll or hide

      jQuery(window).scroll(function() {

        if (jQuery(window).scrollTop() > 1) {

          jQuery(".header-scroll").fadeIn(500);
        } else {

          jQuery(".header-scroll").fadeOut(500);
        }
      });

      // Toggle submenu item

      jQuery(".menu-item-445 .sub-menu").css("display", "none");

      jQuery(".menu-item-446 .sub-menu").css("display", "none");

      jQuery(".menu-item-452 .sub-menu").css("display", "none");

      jQuery(".menu-item-462 .sub-menu").css("display", "none");


      jQuery("#mySidenav .menu-item-445 > a").click(function(){

          jQuery(".menu-item-445 > .sub-menu").slideToggle();
      });

      jQuery("#mySidenav .menu-item-446 > a").click(function(){

          jQuery(".menu-item-446 > .sub-menu").slideToggle();
      });

      jQuery("#mySidenav .menu-item-452 > a").click(function(){

          jQuery(".menu-item-452 > .sub-menu").slideToggle();
      });

      jQuery("#mySidenav .menu-item-462 > a").click(function(){

          jQuery(".menu-item-462 > .sub-menu").slideToggle();
      });

    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</div><!--.header -->