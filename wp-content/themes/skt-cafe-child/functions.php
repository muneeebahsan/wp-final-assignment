<?php
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
 
    $parent_style = 'parent-style';
 
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}

/* Disable Wordpress Builder*/
add_filter('use_block_editor_for_post', '__return_false');


function footer_area_widgets() {

	register_sidebar( array(
		'name' => 'Footer Area 1',
		'id' => 'footer-1',
		'description' => 'Show in the footer area'
	) );

	register_sidebar( array(
		'name' => 'Footer Area 2',
		'id' => 'footer-2',
		'description' => 'Show in the footer area'
	) );

	register_sidebar( array(
		'name' => 'Footer Area 3',
		'id' => 'footer-3',
		'description' => 'Show in the footer area'
	) );

	register_sidebar( array(
		'name' => 'Footer Area 4',
		'id' => 'footer-4',
		'description' => 'Show in the footer area'
	) );
}

add_action( 'widgets_init', 'footer_area_widgets' );