=== SKT Cafe ===

Contributors: SKT Themes
Requires at least: WordPress 4.9  
Tested up to: WordPress 5.0.2
Version: 1.0
License: GPLv2 or later  
License URI: http://www.gnu.org/licenses/gpl-2.0.html  
Tags: two-columns,right-sidebar,custom-background,custom-colors,custom-menu,sticky-post,theme-options,threaded-comments

== Description ==

SKT Cafe is easy, simple, flexible to use for cafe, coffee shop, restaurant, bistro, fast food, recipe, chef, kitchen, beans, tea, eat, drink, pizza delivery, bakery, cafeteria, sushi bars, barbecues, cuisine etc. Can also be used for motel, hotel, lodge, home stay and hospitality business. WordPress 5 gutenberg editor and multilingual WooCommerce friendly. Get documentation at http://sktthemesdemo.net/documentation/cafe-pro-documentation/ and view beautiful demo at https://sktperfectdemo.com/demos/cafe/
 
== Theme Resources == 

Theme is built using the following resource bundles:

* All js that have been used are within folder /js of theme.

*   jQuery Nivo Slider
	Copyright 2012, Dev7studios, under MIT license
	http://nivo.dev7studios.com
MIT License URL: https://opensource.org/licenses/MIT

*   Roboto Condensed - https://www.google.com/fonts/specimen/Roboto+Condensed
	License: Distributed under the terms of the Apache License, version 2.0				
	License URL: http://www.apache.org/licenses/LICENSE-2.0.html
	
*   Roboto - https://fonts.google.com/specimen/Roboto
	License: Distributed under the terms of the Apache License, version 2.0				
	License URL: http://www.apache.org/licenses/LICENSE-2.0.html	
	
*   Great Vibes - https://fonts.google.com/specimen/Great+Vibes
	License: Distributed under the terms of the Apache License, version 2.0				
	License URL: http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL_web	
	
*   Lato - https://fonts.google.com/specimen/Lato
	License: Distributed under the terms of the Apache License, version 2.0				
	License URL: http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL_web
	
*   Open Sans - https://fonts.google.com/specimen/Open+Sans
	License: Distributed under the terms of the Apache License, version 2.0				
	License URL: http://www.apache.org/licenses/LICENSE-2.0.html
	
*   Assistant - https://fonts.google.com/specimen/Assistant
	License: Distributed under the terms of the Apache License, version 2.0				
	License URL: http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL_web	
	
*   Lora - https://fonts.google.com/specimen/Lora
	License: Distributed under the terms of the Apache License, version 2.0				
	License URL: http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL_web	
	
*   Marcellus - https://fonts.google.com/specimen/Marcellus
	License: Distributed under the terms of the Apache License, version 2.0				
	License URL: http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL_web
	
*   Pattaya - https://fonts.google.com/specimen/Pattaya
	License: Distributed under the terms of the Apache License, version 2.0				
	License URL: http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL_web				
	
		
*   SKT Themes Self Designed Images:
	skt-cafe/images/free-vs-pro.png	 
	skt-cafe/images/footer-bg-img.jpg
	skt-cafe/images/coffee-cup-img.png
	skt-cafe/images/home-section1-bg.jpg
	skt-cafe/images/icon-address.png
	skt-cafe/images/icon-fb.png
	skt-cafe/images/icon-gp.png
	skt-cafe/images/icon-in.png
	skt-cafe/images/icon-insta.png
	skt-cafe/images/icon-tw.png
	skt-cafe/images/icon-youtube.png
	skt-cafe/images/loading.gif
	skt-cafe/images/mobile_nav_right.png
	skt-cafe/images/service-img1.png
	skt-cafe/images/service-img2.png
	skt-cafe/images/service-img3.png
	skt-cafe/images/service-img4.png
	skt-cafe/images/sktskill.jpg
	skt-cafe/images/slide-nav.png
	
		
= 	Declaring these self designed images under GPL license version 2.0 =
License URL: http://www.gnu.org/licenses/gpl-2.0.html
		
*   skt-cafe/customize-pro	

		Customize Pro code based on Justintadlock’s Customizer-Pro 
		Source Code URL : https://github.com/justintadlock/trt-customizer-pro			
		License : http://www.gnu.org/licenses/gpl-2.0.html
		Copyright 2016, Justin Tadlock	justintadlock.com
		
*       Details of images used in Screenshot:

		https://pixabay.com/en/coffee-caffeine-espresso-drink-3224527/
		https://pixabay.com/en/coffee-cup-and-saucer-black-coffee-1572739/
		
= 	Declaring these self designed images under GPL license version 2.0 =
		License URL: http://www.gnu.org/licenses/gpl-2.0.html
		
*       Slider Images : 
		skt-cafe/images/Slides/slider1.jpg
		skt-cafe/images/Slides/slider2.jpg
		skt-cafe/images/Slides/slider3.jpg
        
*       Slider Images Are Pixabay Images:  

		https://pixabay.com/en/coffee-caffeine-espresso-drink-3224527/
		https://pixabay.com/en/coffee-bean-cup-coffee-beans-2406443/
		https://pixabay.com/en/coffee-coffee-cup-hot-coffee-steam-2358388/		
		
		Copyright 2017, Pixabay
		https://pixabay.com/ 
		Pixabay provides images under CC0 license
 		CC0 license: https://creativecommons.org/share-your-work/public-domain/cc0			 
        
For any help you can mail us at support[at]sktthemes.com

== Changelog ==

= 1.0 =
*      Initial version release. 