<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp-final-assignment');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@PbKZOe&&IFjp/aJc^Jdy#IM@rXT=}8LY;PKy@ O(6}X,5SoFGmMdloBkJaA(uYq');
define('SECURE_AUTH_KEY',  '8kJtbm@R^O6D~WX8M+SwbQ87Y:mnpfZ2(ROb9#x/?dPs^-39+4T(>|U+Qac%zWKk');
define('LOGGED_IN_KEY',    'f#/<LcFzONdl?|ojq{MXk~u-%9%{e2 ^07<B&E9LOs3]K!MU/PEPg(XkExD)K0x|');
define('NONCE_KEY',        'j96C5OP7f&oC5Fof%.81]CbxTrP6agtOsJ%[@xG8/r$L6l/:@fqR/6Y`)4OlwKyS');
define('AUTH_SALT',        ';=4J?Q-Xcxd[ c;[}m+g1wSo$F(</YUIS3j3ID&&%*=:o2C3V8uTqEJsfm|l.J;8');
define('SECURE_AUTH_SALT', 'vsnYcm{3nRC}PU#Q(<5`]6H$N(f&LitOaiJ;G;(NNcg1+i?pnK%5e}T^:-5-{g}j');
define('LOGGED_IN_SALT',   '%+s^@vxpH0=gy#Y)?%=F#:Vu]TAyiY|mRf~hGd^JRw*UV)+%]3# Omd*d4nbvOcC');
define('NONCE_SALT',       'OiG2.cRKW;XKG4=!tH1zP6J,l.zLIvzF:QWN1%]q#-/4B&%TmHKEyc%=[A[}o{28');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
